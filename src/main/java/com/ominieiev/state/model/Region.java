package com.ominieiev.state.model;

/**
 * Created by ominieiev on 17.09.2015.
 */
public class Region {
     Integer population;
   public String name;

    public Region(Integer population, String name) {
        this.population = population;
        this.name = name;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        } else {
            this.name = "Unknown region";
        }

    }

   public void printWelcome() {
        System.out.println("Welcome to " + this.name);
    }
    public void printLengthName() {
        System.out.println("Our length is " + this.name.length());

    }
}
