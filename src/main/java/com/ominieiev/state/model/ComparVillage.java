package com.ominieiev.state.model;

import java.util.Collection;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * Created by ominieiev on 01.10.2015.
 */
public class ComparVillage implements Comparator,Comparable {
    public int compare(Object o1, Object o2) {
        return 0;
    }

    public Comparator reversed() {
        return null;
    }

    public Comparator thenComparing(Comparator other) {
        return null;
    }

    public Comparator thenComparingInt(ToIntFunction keyExtractor) {
        return null;
    }

    public Comparator thenComparingLong(ToLongFunction keyExtractor) {
        return null;
    }

    public Comparator thenComparingDouble(ToDoubleFunction keyExtractor) {
        return null;
    }

    public Comparator thenComparing(Function keyExtractor) {
        return null;
    }

    public Comparator thenComparing(Function keyExtractor, Comparator keyComparator) {
        return null;
    }

    public int compareTo(Object o) {
        return 0;
    }
}
