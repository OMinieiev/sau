package com.ominieiev.state.model;

/**
 * Created by ominieiev on 17.09.2015.
 */
public abstract class PopulatedPoint {
    Integer population;
    String name;
    Region parentRegion;

    public PopulatedPoint(Integer population, String name) {
        this.population = population;
        this.name = name;
        this.parentRegion=null;
    }

    public PopulatedPoint(Integer population, String name, Region parentRegion) {
        this.population = population;
        this.name = name;
        this.parentRegion = parentRegion;
    }

    protected PopulatedPoint() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PopulatedPoint)) return false;

        PopulatedPoint that = (PopulatedPoint) o;

        if (population != null ? !population.equals(that.population) : that.population != null) return false;
        if (!name.equals(that.name)) return false;
        return parentRegion.equals(that.parentRegion);

    }

    @Override
    public int hashCode() {
        int result = population != null ? population.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + parentRegion.hashCode();
        return result;
    }

    abstract public String printWelcome();
}
