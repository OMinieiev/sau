package com.ominieiev.state.model;

/**
 * Created by ominieiev on 17.09.2015.
 */
public interface CityInstance {
    String populatedPointType="City";

    void payTaxes();
    void makeChoice();

}
